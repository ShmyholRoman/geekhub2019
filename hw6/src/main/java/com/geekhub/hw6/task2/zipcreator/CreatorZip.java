package com.geekhub.hw6.task2.zipcreator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CreatorZip {
    public static void run(String path) throws Exception {
        Map<String, Set<String>> map = new HashMap<>();
        map.put("/video.zip", Set.of(".avi", ".mp4", ".flv"));
        map.put("/audio.zip", Set.of(".mp3", ".wav", ".wma"));
        map.put("/image.zip", Set.of(".jpeg", ".jpg", ".gif", ".png"));
        File input = new File(path);
        for (Map.Entry<String, Set<String>> entry : map.entrySet()) {
            List<File> list = new ArrayList<>();
            List<File> tmp;
            for (String format : entry.getValue()) {

                Stream<Path> walk = Files.walk(Paths.get(path));
                tmp = walk.map(x -> new File(x.toString()))
                        .filter(f -> f.getPath().endsWith(format))
                        .collect(Collectors.toList());

                list.addAll(tmp);
            }

            try (ZipOutputStream zipOutputStream = new ZipOutputStream(
                    new FileOutputStream(new File(input.getParent() + entry.getKey())))) {
                createZip(list, input, zipOutputStream);
            }
        }
    }

    private static void createZip(List<File> files, File inputFile, ZipOutputStream zipOutputStream) throws Exception {
        for (File file : files) {
            String filePath = file.getCanonicalPath();
            int lengthDirectoryPath = inputFile.getCanonicalPath().length();
            int lengthFilePath = file.getCanonicalPath().length();
            String zipFilePath = filePath.substring(lengthDirectoryPath + 1, lengthFilePath);
            ZipEntry zipEntry = new ZipEntry(zipFilePath);
            zipOutputStream.putNextEntry(zipEntry);
            try (InputStream inputStream = new FileInputStream(file)) {
                byte[] bytes = new byte[1024];
                int length;
                while ((length = inputStream.read(bytes)) >= 0) {
                    zipOutputStream.write(bytes, 0, length);
                }
                zipOutputStream.closeEntry();
            }
        }
    }
}
