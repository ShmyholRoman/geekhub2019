package com.geekhub.hw6.task2;

import com.geekhub.hw6.task2.zipcreator.CreatorZip;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ZipFile {

    public static void main(String[] args) throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Input path of folder: ");
            String path = reader.readLine();
            if (path.isEmpty()) {
                throw new RuntimeException("Don't have path of file!" + "\n" + "Please, input path!");
            }
            CreatorZip.run(path);
        }
    }
}
