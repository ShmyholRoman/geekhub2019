package com.geekhub.hw6.task1;

import java.io.File;
import java.util.StringTokenizer;

public class FileEncryption {
    public static void main(String[] args) throws Exception {
        IOOperation operation = new IOOperation();
        StringBuilder results = new StringBuilder();
        String nameFile = new File("hw6/src/main/resources/text.txt").getCanonicalPath();
        File file = new File(nameFile);
        String reader;
        reader = operation.reader(file);
        StringTokenizer words = new StringTokenizer(reader);
        while (words.hasMoreTokens()) {
            String word = words.nextToken();
            int lengthOfElement = word.length();
            if (lengthOfElement >= 10) {
                if (word.contains(".") || word.contains(",")) {
                    results.append(word, 0, 1).append(lengthOfElement - 2).append(word.substring(lengthOfElement - 2)).append(" ");
                } else {
                    results.append(word, 0, 1).append(lengthOfElement - 2).append(word.substring(lengthOfElement - 1)).append(" ");

                }
            } else {
                results.append(word).append(" ");
            }

        }

        operation.writer(results.toString());
    }
}
