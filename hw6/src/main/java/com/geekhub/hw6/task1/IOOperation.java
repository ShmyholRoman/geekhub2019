package com.geekhub.hw6.task1;

import java.io.*;

public class IOOperation {
    public String reader(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s;
            StringBuilder str = new StringBuilder();
            while ((s = reader.readLine()) != null) {
                str.append(s);
            }
            return str.toString();
        }

    }

    public void writer(String results) throws IOException {
        String path = new File("hw6/src/main/resources/result.txt").getCanonicalPath();
        File result = new File(path);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(result.getAbsoluteFile()))) {
            writer.write(results);
        }

    }
}
