create table _cat
(
    id   serial       not null
        constraint cat_pk
            primary key,
    name varchar(100) not null,
    age  integer      not null
);

alter table _cat
    owner to postgres;

create unique index cat_id_uindex
    on _cat (id);

create table _user
(
    id      serial           not null
        constraint user_pk
            primary key,
    name    varchar(100)     not null,
    age     integer          not null,
    admin   boolean          not null,
    balance double precision not null
);

alter table _user
    owner to postgres;

create unique index user_id_uindex
    on _user (id);

