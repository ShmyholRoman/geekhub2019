create table customer
(
    id        serial not null,
    firstname varchar(100),
    lastname  varchar(100),
    cellphone varchar(15)
);

alter table customer
    owner to postgres;

create table orders
(
    id            serial       not null
        constraint order_pk
            primary key,
    id_customer   integer      not null
        constraint id_customer
            references customer,
    id_product    integer      not null
        constraint id_product
            references product,
    deliveryplace varchar(250) not null,
    quantity      integer      not null,
    price         integer
);

alter table orders
    owner to postgres;

create table product
(
    id           serial not null
        constraint product_pk
            primary key,
    name         varchar(100),
    description  varchar(300),
    currentprice integer
);

alter table product
    owner to postgres;

create unique index product_id_uindex
    on product (id);

