/*
--№1
select c.*, count(s.id) count
from countries c left join states s on s.country_id = c.id
group by c.id
order by count desc limit 1;
*/

/*
--№2
select c.*, count(ct.id) count
from countries c left join states s on s.country_id = c.id
                 left join cities ct on ct.state_id = s.id
group by c.id
order by count desc limit 1;
*/

/*№3
select c.*, count(s.id) count
from countries c left join states s on s.country_id = c.id
-- group by c.id
-- order by count desc;
group by c.id, c.name
order by c.name;
*/

/*№4
select c.*, count(ct.id) count
from countries c left join states s on s.country_id = c.id
                 left join cities ct on ct.state_id = s.id
group by c.id, c.name
-- order by count desc;
order by c.name;
*/

/*№5
select c.*, count(distinct s.id) count_states, count(ct.id) count_city
from countries c left join states s on s.country_id = c.id
                 left join cities ct on ct.state_id = s.id
group by c.id;
*/

/*№6
select c.*, s.name, count(ct.id) count_city
from countries c left join states s on s.country_id = c.id
                 left join cities ct on ct.state_id = s.id
group by c.id,s.name
order by count_city desc limit 10;
*/

/*№7
(select c.*, s.name, count(ct.id) count_city
from countries c left join states s on s.country_id = c.id
                 left join cities ct on ct.state_id = s.id
group by c.id,s.name
order by count_city desc limit 10)
union all 
(select c.*, s.name, count(ct.id) count_city
 from countries c left join states s on s.country_id = c.id
                  left join cities ct on ct.state_id = s.id
 group by c.id,s.name
 order by count_city limit 10)
*/

/*№8
select c.* from
(select c.id, count(s.id) counts
 from countries c left join states s on s.country_id = c.id
 group by c.id) n, countries c left join states s2 on c.id = s2.country_id
group by c.id
having count(distinct s2.id) > avg(counts);
*/

/*№9
select distinct on (count(s.id)) c.*, count(s.id) as counts
from countries c left join states s on s.country_id = c.id
group by c.id,c.name;
*/

/*№10
select s.*
from states s
where s.name not in (select s2.name 
                     from states s2 )
*/

/*№11
select distinct (s.*)
from states s
where s.id not in (select c.state_id from cities c);
*/
