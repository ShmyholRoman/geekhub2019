/*insert data in station db

insert into train_db.station (id_pasenger, id_tikets,id_direction, id_train) values((select id from train_db.pasenger number_passport = ere35533),(select id from train_db.direction where name = 'Odessa'), (select id from train_db.train where name = 'k123'));
insert into train_db.first_name (id,first_name) values(32,"Roman");
insert into train_db.last_name (id,last_name) values (1,'Shmyhol');
insert into train_db.age (id,age) values (3first_nameidfirst_name,20);
insert into train_db.pasenger (id,id_first_name,id_last_name, age, number_passport)
values(5,(select id from train_db.first_name where first_name = 'Roman'),
	   (select id from train_db.last_name where last_name = 'Shmyhol'),
       (select id from train_db.age where age = 20),'ere35533');

insert into train_db.direction(id,name) values(23,"Odessa");
insert into train_db.train (id,name,quantity) values(4343, "k123", 4345);
insert into train_db.station (id,id_pasenger, id_tikets,id_direction, id_train) values(323,(select id from train_db.pasenger where number_passport = "ere35533"),(select id from train_db.tiket where id_direction = 23),(select id from train_db.direction where name = 'Odessa'), (select id from train_db.train where name = 'k123'));
insert into train_db.tiket (id, name,id_direction) values(543,"abs",23);
*/

/* correct name of pasenger in db
update train_db.pasenger
set id_first_name = (select f.id from train_db.first_name f where first_name = "Andrew")
where number_passport = "ere35533";
*/

/* correct direction of train
update train_db.station
set id_direction = (select d.id from train_db.direction d where name = "Kiyv")
where id_train = (select t.id from train_db.train t where name = "k123");
*/

/* correct quantity of tikets on train
update train_db.train
set quantity = quantity - 1
where (select t.id from train_db.tiket t where name = "abs")
		=
	  (select s.id_tikets from train_db.station s where id_train = 4343);

*/

/* select average quantity of tikets by train
select s.*, avg(t.quantity) as avg
from train_db.station s left join train_db.train t on t.id = s.id_train
group by s.id;
*/


/* show pasenger on train by age < 20
select t.name, count(p.age)
from train_db.train t left join train_db.pasenger p on p.age < 20
group by t.name;
*/

/* count of pasenger that go on train by direction
select d.id_direction, count(p.name) as count, p.name
from train_db.station d left join train_db.direction p on d.id_direction = p.id
group by d.id_direction;
*/

/* show count of trip from this station by pasenger
select d.id_direction, count(p.name) as count, p.name
from train_db.station d left join train_db.direction p on d.id_direction = p.id
group by d.id_direction;
*/

/* show count of  last name for show common last name by pasenger
select p.*, count(l.last_name)
from train_db.pasenger p left join train_db.last_name l on l.id = p.id_last_name
group by p.id;
*/

/* delete all pasenger from db who ages equals more 100
delete from train_db.pasenger p where p.age > 100;
*/

/* delete pasenger from db who isn't in db station
delete from train_db.pasenger p where p.id not in (select s.id_pasenger from train_db.station s);
*/