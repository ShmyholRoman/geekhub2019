CREATE TABLE `first_name` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `first_name_UNIQUE` (`first_name`)
);

CREATE TABLE `age` (
  `id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `age_UNIQUE` (`age`)
);

CREATE TABLE `direction` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
);

CREATE TABLE `last_name` (
  `id` int(11) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idlast_name_UNIQUE` (`id`),
  UNIQUE KEY `last_name_UNIQUE` (`last_name`)
);

CREATE TABLE `pasenger` (
  `id` int(11) NOT NULL,
  `id_first_name` int(11) NOT NULL,
  `id_last_name` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `number_passport` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `number_passport_UNIQUE` (`number_passport`),
  KEY `id_first_name_idx` (`id_first_name`),
  KEY `id_last_name_idx` (`id_last_name`),
  KEY `id_age_idx` (`age`),
  CONSTRAINT `id_age` FOREIGN KEY (`age`) REFERENCES `age` (`id`),
  CONSTRAINT `id_first_name` FOREIGN KEY (`id_first_name`) REFERENCES `first_name` (`id`),
  CONSTRAINT `id_last_name` FOREIGN KEY (`id_last_name`) REFERENCES `last_name` (`id`)
);

CREATE TABLE `station` (
  `id` int(11) NOT NULL,
  `id_pasenger` int(11) NOT NULL,
  `id_tikets` int(11) NOT NULL,
  `id_direction` int(11) NOT NULL,
  `id_train` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pasenger_idx` (`id_pasenger`),
  KEY `id_tikets_idx` (`id_tikets`),
  KEY `id_direction_idx` (`id_direction`),
  KEY `id_train_idx` (`id_train`),
  CONSTRAINT `id_direction` FOREIGN KEY (`id_direction`) REFERENCES `direction` (`id`),
  CONSTRAINT `id_pasenger` FOREIGN KEY (`id_pasenger`) REFERENCES `pasenger` (`id`),
  CONSTRAINT `id_tikets` FOREIGN KEY (`id_tikets`) REFERENCES `tiket` (`id`),
  CONSTRAINT `id_train` FOREIGN KEY (`id_train`) REFERENCES `train` (`id`)
);

CREATE TABLE `tiket` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_direction` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `id_direction_idx` (`id_direction`)
);

CREATE TABLE `train` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `quantity` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
);



