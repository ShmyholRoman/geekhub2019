package com.geekhub.hw8.task2.storage;


import com.geekhub.hw8.task2.objects.DBName;
import com.geekhub.hw8.task2.objects.Entity;
import com.geekhub.hw8.task2.objects.Ignore;
import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private DataSource dataSource;

    public DatabaseStorage(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        DBName db = clazz.getAnnotation(DBName.class);
        String sql = "SELECT * FROM " + db.name() + " WHERE id = " + id + ";";
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            if (!result.isEmpty()) {
                result.get(0).setId(id);
            }
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        DBName db = clazz.getAnnotation(DBName.class);
        String sql = "SELECT * FROM " + db.name() + ";";
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? new ArrayList<>() : result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        DBName db = entity.getClass().getAnnotation(DBName.class);
        String sql = "DELETE FROM " + db.name()
                + " WHERE id = " + entity.getId() + ";";
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            int count = statement.executeUpdate(sql);
            if (count > 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        DBName db = entity.getClass().getAnnotation(DBName.class);
        StringBuilder field = new StringBuilder();
        StringBuilder fieldValues = new StringBuilder();
        StringBuilder fieldForUpdate = new StringBuilder();

        Map<String, Object> data = prepareEntity(entity);
        ArrayList<Object> list = new ArrayList<>();
        data.forEach((key, value) -> {
            field.append(key).append(",");
            fieldForUpdate.append(key).append("=?,");
            fieldValues.append("?" + ",");
            list.add(value);
        });
        field.deleteCharAt(field.length() - 1);
        fieldValues.deleteCharAt(fieldValues.length() - 1);
        fieldForUpdate.deleteCharAt(fieldForUpdate.length() - 1);

        if (entity.isNew()) {
            String sql = "INSERT INTO " + db.name() + " (" + field + ") " + " VALUES(" + fieldValues + ")" + ";";
            try(Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)){
                for (int i = 0; i < data.size(); i++) {
                    statement.setObject(i + 1, list.get(i));
                }
                statement.executeUpdate();
                ResultSet generatedKeys = statement.getGeneratedKeys();
                while (generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt("id"));
                }
            }
        } else {
            String sql = "UPDATE " + db.name() + " SET " + fieldForUpdate + " WHERE id =" + entity.getId() + ";";
            try(Connection connection = dataSource.getConnection();
                PreparedStatement statement = connection.prepareStatement(sql)){
                for (int i = 0; i < data.size(); i++) {
                    statement.setObject(i + 1, list.get(i));
                }
                statement.executeUpdate();
            }
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) continue;
            field.setAccessible(true);
            data.put(field.getName(), field.get(entity));
        }

        return data;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> result = new ArrayList<>();
        T object = clazz.newInstance();
        Field[] fields = object.getClass().getDeclaredFields();
        while (resultSet.next()) {
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.set(object, resultSet.getObject(field.getName()));
            }
            result.add(object);
        }

        return result;
    }
}
