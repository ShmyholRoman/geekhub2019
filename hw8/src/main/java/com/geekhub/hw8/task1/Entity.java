package com.geekhub.hw8.task1;

public class Entity {
    private Integer id;

    public boolean isNew() {
        return getId() == null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
