package com.geekhub.hw8.task1;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Crud {
    private Connection connection;

    public Crud(Connection connection) {
        this.connection = connection;
    }

    public <T extends Entity> void addToDB(T object) throws Exception {
        String nameTable = object.getClass().getSimpleName().toLowerCase();
        StringBuilder field = new StringBuilder();
        StringBuilder fieldValues = new StringBuilder();
        StringBuilder fieldForUpdate = new StringBuilder();
        Map<String, Object> data = prepareEntity(object);
        ArrayList<Object> list = new ArrayList<>();
        data.forEach((key, value) -> {
            field.append(key).append(",");
            fieldForUpdate.append(key).append("=?,");
            fieldValues.append("?" + ",");
            list.add(value);
        });
        field.deleteCharAt(field.length() - 1);
        fieldValues.deleteCharAt(fieldValues.length() - 1);
        fieldForUpdate.deleteCharAt(fieldForUpdate.length() - 1);
        if (object.isNew()) {
            String sql = "INSERT INTO " + nameTable + " (" + field + ") " + " VALUES(" + fieldValues + ")" + ";";
            try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                for (int i = 0; i < data.size(); i++) {
                    statement.setObject(i + 1, list.get(i));
                }
                statement.executeUpdate();
                ResultSet generatedKeys = statement.getGeneratedKeys();
                while (generatedKeys.next()) {
                    object.setId(generatedKeys.getInt("id"));
                }
            }
        } else {
            String sql = "UPDATE " + nameTable + " SET " + fieldForUpdate + " WHERE id =" + object.getId() + ";";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                for (int i = 0; i < data.size(); i++) {
                    statement.setObject(i + 1, list.get(i));
                }
                statement.executeUpdate();
            }
        }
    }

    private <T> Map<String, Object> prepareEntity(T object) throws Exception {
        Map<String, Object> data = new HashMap<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            data.put(field.getName(), field.get(object));
        }

        return data;
    }

    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName().toLowerCase()
                + " WHERE id = " + entity.getId() + ";";

        try (Statement statement = connection.createStatement()) {
            int count = statement.executeUpdate(sql);
            if (count > 0) {
                return true;
            }
        }

        return false;
    }

    public boolean addToOrder(Customer customer, Product product, String deliveryPlace, int quantity) throws SQLException {
        String sql = "INSERT INTO orders (id_customer, id_product, deliveryplace, quantity, price) VALUES (?,?,?,?,?)";


        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, customer.getId());
            preparedStatement.setInt(2, product.getId());
            preparedStatement.setString(3, deliveryPlace);
            preparedStatement.setInt(4, quantity);
            preparedStatement.setInt(5, quantity * product.getCurrentPrice());

            int count = preparedStatement.executeUpdate();
            if (count > 0) {
                return true;
            }
        }

        return false;
    }

    public void showTotalPriceByCustomer() throws SQLException {
        String sql = "SELECT distinct (c.*), SUM(o.price) " +
                "FROM customer c " +
                "LEFT JOIN orders o " +
                "ON c.id = o.id_customer " +
                "GROUP BY c.id,c.firstname;";
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            StringBuilder str = new StringBuilder();
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    str.append(resultSet.getString(i)).append("|");
                }
                System.out.println(str);
                str.delete(0, str.length());
            }
        }
    }

    public String showPopularProduct() throws SQLException {
        String sql = "SELECT pr.id,pr.name, MAX(o.quantity) AS popular_product " +
                "FROM product pr " +
                "LEFT JOIN orders o " +
                "ON pr.id = o.id_product " +
                "GROUP BY pr.id, pr.name " +
                "ORDER BY popular_product desc ;";
        List<String> list = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    stringBuilder.append(resultSet.getString(i)).append(" ");
                }
                list.add(stringBuilder.toString());
            }
        }

        return list.get(0);
    }
}
