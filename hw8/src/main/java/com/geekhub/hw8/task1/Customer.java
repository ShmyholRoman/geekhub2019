package com.geekhub.hw8.task1;

public class Customer extends Entity {
    private String firstName;
    private String lastName;
    private String cellPhone;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }
}
