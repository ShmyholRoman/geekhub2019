package com.geekhub.hw8.task1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;


public class Test {
    private static final String dbName = "jdbc:postgresql://localhost:5432/store";
    private static final String username = "postgres";
    private static final String password = "";

    public static void main(String[] args) throws Exception {
        Connection connection = createConnection(username, password, dbName);
        Crud crud = new Crud(connection);
        Customer customer = new Customer();
        customer.setFirstName("first name 1");
        customer.setLastName("last name 1");
        customer.setCellPhone("+423453543234");
        Customer customer1 = new Customer();
        customer1.setFirstName("first name 2");
        customer1.setLastName("last name 2");
        customer1.setCellPhone("+453543234");
        Customer customer2 = new Customer();
        customer2.setFirstName("first name 3");
        customer2.setLastName("last name 3");
        customer2.setCellPhone("+423453544");
        crud.addToDB(customer);
        crud.addToDB(customer1);
        crud.addToDB(customer2);

        Product product = new Product();
        product.setName("Product1");
        product.setCurrentPrice(123);
        product.setDescription("Product1,...");

        Product product1 = new Product();
        product1.setName("Product2");
        product1.setCurrentPrice(32);
        product1.setDescription("Product2,...");

        crud.addToDB(product);
        crud.addToDB(product1);

        crud.addToOrder(customer, product1, "Place of delivery", 12);
        crud.addToOrder(customer2, product, "Place of delivery2", 2);
        crud.addToOrder(customer1, product1, "Place of delivery2", 2);
        System.out.println("Client with total amount of spent money: ");
        crud.showTotalPriceByCustomer();

        System.out.println();

        System.out.printf("Most popular product: %s", crud.showPopularProduct());


        connection.close();
    }

    private static Connection createConnection(String login, String password, String dbName) throws Exception {
        return DriverManager.getConnection(dbName, login, password);
    }


}
