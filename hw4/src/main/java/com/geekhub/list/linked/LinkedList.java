package com.geekhub.list.linked;

import com.geekhub.list.List;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {
    private Node<E> head;
    private int size = 0;

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        Node<E> current = head;

        if (head == null) {
            head = node;
        } else {
            while (current.next != null) {
                current = current.next;
            }
            current.next = node;
        }
        size++;

        return true;
    }

    @Override
    public boolean add(int index, E element) {
        Node<E> node = new Node<>(element, null);
        Node<E> current = head;
        int i = 0;
        if (index == 0) {
            node.next = head;
            head = node;
            size++;
        } else {
            while (i < index - 1) {
                current = current.next;
                i++;
            }
            node.next = current.next;
            current.next = node;
            size++;
        }

        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        Iterator<E> iterator = elements.iterator();
        while (iterator.hasNext()) {
            Node<E> node = new Node<>(iterator.next(), null);
            Node<E> current = head;

            if (head == null) {
                head = node;
            } else {
                while (current.next != null) {
                    current = current.next;
                }
                current.next = node;
            }
            size++;
        }

        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        Iterator<E> iterator = elements.iterator();
        while (iterator.hasNext()) {
            Node<E> node = new Node<>(iterator.next(), null);
            Node<E> current = head;
            int i = 0;
            if (index == 0) {
                node.next = head;
                head = node;
                size++;
            } else {
                while (i < index - 1) {
                    current = current.next;
                    i++;
                }
                node.next = current.next;
                current.next = node;
                size++;
            }
            index++;
        }

        return true;
    }

    @Override
    public boolean clear() {
        head = null;
        size = 0;

        return true;
    }

    @Override
    public E remove(int index) {
        E result;
        int i = 0;
        Node<E> current = head;
        Node<E> node = new Node<>(null, null);
        if (index == 0) {
            result = head.element;
            head = current.next;
            size--;
            return result;
        }
        if (index == 1) {
            result = head.next.element;
            head.next = head.next.next;
            size--;
            return result;
        }
        while (i < index) {
            node = current;
            current = current.next;
            i++;
        }
        node.next = current.next;
        result = node.element;
        size--;

        return result;
    }

    @Override
    public E remove(E element) {
        E result = null;
        Node<E> cur = head;
        Node<E> pre = new Node<>(null, head);
        if (cur.element.equals(element)) {
            result = head.element;
            head = head.next;
            size--;
            return result;
        }
        while (cur.next != null) {
            if (cur.element.equals(element)) {
                pre.next = cur.next;
                cur = cur.next;
                result = element;
            } else {
                pre = cur;
                cur = pre.next;
                result = null;
            }
        }
        size--;

        return result;
    }

    @Override
    public E get(int index) {
        Node<E> current = head;
        int count = 0;
        while (current.element != null) {
            if (count == index) {
                return current.element;
            }
            count++;
            current = current.next;
        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int indexOf(E element) {
        int index = 0;
        Node<E> current = head;

        while (current != null) {
            if (current.element.equals(element)) {
                return index;
            }
            index++;
            current = current.next;
        }

        return -1;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }
}