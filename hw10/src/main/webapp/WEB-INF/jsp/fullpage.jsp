<%--
  Created by IntelliJ IDEA.
  User: roman
  Date: 1/29/20
  Time: 9:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Guest Book</title>
</head>
<body>
<form action="/full" method="post">
    <input name="name" placeholder="Name" required="required"> <br/>
    <textarea name="comment" maxlength="255" rows="5" cols="49" placeholder="Comment" required="required"></textarea> <br/>
    rating
    <select name="rating">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
    </select>
    <button type="submit">Submit</button>
</form>
<br/> <br/>
<c:forEach var="feedback" items="${feedbackList}">
    Name: <c:out value="${feedback.name}"/>, Rating : <c:out value="${feedback.rating}"/> <br/>
    Comment: <c:out value="${feedback.comment}"/> <br/>
</c:forEach>
</body>
</html>