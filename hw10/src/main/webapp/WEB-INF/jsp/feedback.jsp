<%--
  Created by IntelliJ IDEA.
  User: Vitalii Harchenko
  Date: 06.12.2019
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
  <title>Guest Book</title>
</head>
<body>
For keep comment log in to system! <a href="<c:url value="/loginto" />">Log In</a><br>
<c:forEach var="feedback" items="${feedbackList}">
  Name: <c:out value="${feedback.name}"/>, Rating : <c:out value="${feedback.rating}"/> <br/>
  Comment: <c:out value="${feedback.comment}"/> <br/>
</c:forEach>
</body>
</html>
