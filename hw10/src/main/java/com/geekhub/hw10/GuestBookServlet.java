package com.geekhub.hw10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/feedback")
public class GuestBookServlet extends HttpServlet {
    private FeedbackDAO feedbackDAO = new FeedbackDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Feedback> feedbackList = feedbackDAO.getFeedbackItems();
        req.setAttribute("feedbackList", feedbackList);
        req.getRequestDispatcher("WEB-INF/jsp/feedback.jsp")
                .forward(req, resp);
    }

}
