package com.geekhub.hw10;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDAO {
    public void insertFeedback(Feedback feedback) {
        String name = feedback.getName();
        String comment = feedback.getComment();
        int rating = feedback.getRating();
        LocalDateTime date = feedback.getDate();

        final String query = "INSERT INTO feedback (username, comment_fb, rating, date) " +
                "VALUES ('" + name + "', '" + comment + "', '" + rating + "', '" + date + "');";

        Connection conn = ConnectionManager.getConnection();
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Feedback> getFeedbackItems() {
        List<Feedback> feedbackList = new ArrayList<>();
        final String query = "SELECT username, comment_fb, rating, date " +
                "FROM feedback " +
                "ORDER BY date DESC;";

        Connection conn = ConnectionManager.getConnection();
        try (Statement stmt = conn.createStatement()) {
            ResultSet resultSet = stmt.executeQuery(query);

            while (resultSet.next()) {
                Feedback feedback = new Feedback();
                feedback.setName(resultSet.getString("username"));
                feedback.setComment(resultSet.getString("comment_fb"));
                feedback.setRating(resultSet.getInt("rating"));
                feedback.setDate(resultSet.getTimestamp("date").toLocalDateTime());
                feedbackList.add(feedback);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return feedbackList;
    }
}
