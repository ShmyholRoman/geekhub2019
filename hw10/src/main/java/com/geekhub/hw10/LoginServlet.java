package com.geekhub.hw10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/loginto")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/login.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Map<String, String> logAndPass = new HashMap<>();
        logAndPass.put("root", "12345");
        logAndPass.put("admin", "admin");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        if (logAndPass.containsKey(login) && logAndPass.get(login).equals(password)) {
            resp.sendRedirect("/full");

        } else {
            resp.getWriter().print("<h1>Error incorrect login or password!!!");
            resp.sendRedirect("/loginto");
        }
    }
}
