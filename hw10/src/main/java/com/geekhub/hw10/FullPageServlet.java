package com.geekhub.hw10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@WebServlet("/full")
public class FullPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        List<Feedback> feedbackList = feedbackDAO.getFeedbackItems();
        req.setAttribute("feedbackList", feedbackList);
        req.getRequestDispatcher("WEB-INF/jsp/fullpage.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        String comment = req.getParameter("comment");
        int rating = Integer.parseInt(req.getParameter("rating"));

        Feedback feedback = new Feedback();
        feedback.setName(name);
        feedback.setComment(comment);
        feedback.setRating(rating);
        feedback.setDate(LocalDateTime.now());
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        feedbackDAO.insertFeedback(feedback);

        resp.sendRedirect("/full");
    }
}
