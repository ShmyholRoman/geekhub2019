package com.geekhub.hw1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TelephoneNumber {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number, count;
        String strnumber, prefixnumber;

        while (true) {
            System.out.println("Please enter the phone number:");
            strnumber = reader.readLine();
            prefixnumber = strnumber.substring(0, 3);

            if (strnumber.length() == 10 && PrefixOfNumber(prefixnumber)) {
                System.out.println("Phone number is correct.");
                break;
            } else {
                System.out.print("Phone number is incorrect.  ");
            }
        }

        count = strnumber.length();
        number = Integer.parseInt(strnumber);
        int i = 1;

        while (count != 1) {
            String tmp;
            number = sumOfNumbers(number);
            System.out.println(i + "st round of calculation, sum is: " + number);
            i = i + 1;
            tmp = Integer.toString(number);
            count = tmp.length();
        }

        System.out.print("Final result is: ");
        result(number);
    }

    private static boolean PrefixOfNumber(String prefix) {
        String[] variousofprefix = {"067", "097", "066", "050", "096", "068", "063", "073", "093", "099"};

        for (String s : variousofprefix) {
            if (s.equals(prefix)) {
                return true;
            }
        }

        return false;
    }

    private static int sumOfNumbers(int number) {
        int sum = 0;

        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }

        return sum;
    }

    private static void result(int a) {
        switch (a) {
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Tree");
                break;
            case 4:
                System.out.println("Four");
                break;
            default:
                System.out.println(a);
        }
    }
}