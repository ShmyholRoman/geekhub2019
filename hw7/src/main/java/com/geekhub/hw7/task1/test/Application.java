package com.geekhub.hw7.task1.test;

import com.geekhub.hw7.task1.reflection.BeanComparator;
import com.geekhub.hw7.task1.reflection.BeanRepresenter;
import com.geekhub.hw7.task1.reflection.CloneCreator;

public class Application {

    public static void main(String[] args) throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        Cat cat = new Cat("Black", 3, 4, 35);
        Car car = new Car("black", 190, "Sedan", "RX-7");
        Human human = new Human(180, "male", 22, 75);
        System.out.println("*** Test of BeanRepresenter ***");
        System.out.println();
        BeanRepresenter beanRepresenter = new BeanRepresenter();
        beanRepresenter.representObject(cat);
        System.out.println();
        beanRepresenter.representObject(car);
        System.out.println();
        beanRepresenter.representObject(human);
        System.out.println();
        System.out.println("*** Test of CloneCreator ***");
        System.out.println();
        CloneCreator cloneCreator = new CloneCreator();
        Cat cloneCat = cloneCreator.createClone(cat);
        System.out.println("Original");
        System.out.println(cat.toString());
        System.out.println("Clone");
        System.out.println(cloneCat.toString());
        System.out.println();
        cat.setAge(23);
        cloneCat.setAge(65);
        System.out.println("*** Correct original age on 23 and correct clone age on 65 ***");
        System.out.println();
        System.out.println("Original");
        System.out.println(cat.toString());
        System.out.println("Clone");
        System.out.println(cloneCat.toString());
        System.out.println();
        System.out.println("*** Test of BeanComparator ***");
        System.out.println();
        BeanComparator<Cat> beanComparator = new BeanComparator<>();
        beanComparator.representComparator(cat, cloneCat);
    }
}
