package com.geekhub.hw7.task1.reflection;

import java.lang.reflect.Field;

public class BeanComparator<T> {

    public void representComparator(T object1, T object2) throws IllegalAccessException {
        Class clazz1 = object1.getClass();
        Field[] fieldsOfObject1 = clazz1.getDeclaredFields();
        String format = "%-15s%-15s%-15s%s%n";
        System.out.printf(format, "Variable", "Class1", "Class2", "Match");
        for (Field field : fieldsOfObject1) {
            field.setAccessible(true);
            String nameField = field.getName();
            String valueClass1 = field.get(object1).toString();
            String valueClass2 = field.get(object2).toString();
            boolean comparatorValue = field.get(object1).equals(field.get(object2));
            System.out.printf(format, nameField, valueClass1, valueClass2, comparatorValue);
        }
    }
}
