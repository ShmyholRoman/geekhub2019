package com.geekhub.hw7.task1.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CloneCreator {

    public <T> T createClone(T o) throws IllegalAccessException, InstantiationException {
        Class clazz = o.getClass();
        Field[] fields = clazz.getDeclaredFields();
        Method[] methods = clazz.getDeclaredMethods();
        T clone = (T) clazz.newInstance();
        for (Field field : fields) {
            field.setAccessible(true);
            field.set(clone, field.get(o));
        }
        for (Method method : methods) {
            method.setAccessible(true);
        }

        return clone;
    }
}