package com.geekhub.hw7.task1.reflection;

import java.lang.reflect.Field;

public class BeanRepresenter {

    public void representObject(Object o) throws ClassNotFoundException, IllegalAccessException {
        String nameClass = o.getClass().getName();
        Class clazz = Class.forName(nameClass);
        Field[] fields = clazz.getDeclaredFields();
        System.out.println(o.getClass().getSimpleName());
        for (Field field : fields) {
            field.setAccessible(true);
            String format = "%-15s%s%n";
            String nameField = field.getName();
            String value = field.get(o).toString();
            System.out.printf(format, nameField, value);
        }
    }
}
