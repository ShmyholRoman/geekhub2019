package com.geekhub.hw7.task1.test;

public class Cat {
    private int age;
    private String color;
    private int legCount;
    private int fullLength;

    public Cat() {
    }

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", fullLength=" + fullLength +
                '}';
    }

    public void setAge(int age) {
        this.age = age;
    }
}
