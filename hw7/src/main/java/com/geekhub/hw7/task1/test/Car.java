package com.geekhub.hw7.task1.test;

public class Car {
    private String color;
    private double maxSpeed;
    private String type;
    private String model;

    public Car() {
    }

    public Car(String color, double maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
