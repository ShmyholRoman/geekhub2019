package com.geekhub.hw7.task1.test;

public class Human {
    private double height;
    private String male;
    private int age;
    private int weight;

    public Human() {
    }

    public Human(double height, String male, int age, int weight) {
        this.height = height;
        this.male = male;
        this.age = age;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Human{" +
                "height=" + height +
                ", male='" + male + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}
