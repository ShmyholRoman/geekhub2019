package com.geekhub.hw7.task2.json.adapter;

import com.geekhub.hw7.task2.json.JsonSerializer;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */
public class MapAdapter implements JsonDataAdapter<Map> {

    @Override
    public Object toJson(Map map) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        map.forEach((key, value) ->
                jsonObject.put(String.valueOf(JsonSerializer.serialize(key)), JsonSerializer.serialize(value))
        );

        return jsonObject;
    }
}
