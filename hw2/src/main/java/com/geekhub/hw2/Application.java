package com.geekhub.hw2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.geekhub.hw2.Shapes.*;

public class Application {

    private static String shape;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        boolean correct = false;

        while (!correct) {
            System.out.println("Please, enter the Shapes (CIRCLE ,SQUARE, RECTANGLE, TRIANGLE): ");
            shape = reader.readLine();
            shape = shape.toUpperCase();
            if (shape.equals(CIRCLE.toString()) || shape.equals(RECTANGLE.toString())
                    || shape.equals(SQUARE.toString()) || shape.equals(TRIANGLE.toString())) {
                correct = true;
            } else {
                System.out.println("Incorrect, please enter again. ");
            }
        }
        Shapes shapes = Shapes.valueOf(shape);

        switch (shapes) {
            case CIRCLE: {
                System.out.println("Please enter the radius of circle: ");
                double r = Double.parseDouble(reader.readLine());
                Circle circle = new Circle(r);
                System.out.println("Area of circle = " + circle.calculateArea());
                System.out.println("Perimeter of circle = " + circle.calculatePerimeter());
                break;
            }
            case SQUARE: {
                System.out.println("Please enter the side of square");
                double a = Double.parseDouble(reader.readLine());
                Square square = new Square(a);
                Triangle triangle = new Triangle(a, a, Math.sqrt(Math.pow(a, 2) + Math.pow(a, 2)));
                System.out.println("Area of square = " + square.calculateArea());
                System.out.println("Perimeter of square = " + square.calculatePerimeter());
                System.out.println("Area from 2 triangles = " + triangle.calculateArea());
                System.out.println("Perimeter from 2 triangles = " + triangle.calculatePerimeter());
                break;
            }
            case TRIANGLE: {
                System.out.println("Please enter the sides a,b,c of triangle:");
                double a = Double.parseDouble(reader.readLine());
                double b = Double.parseDouble(reader.readLine());
                double c = Double.parseDouble(reader.readLine());
                Triangle triangle = new Triangle(a, b, c);
                System.out.println("Area of triangle = " + triangle.calculateArea());
                System.out.println("Perimeter of triangle = " + triangle.calculatePerimeter());
                break;
            }
            case RECTANGLE: {
                System.out.println("Please enter the length and width of rectangle: ");
                double length = Double.parseDouble(reader.readLine());
                double width = Double.parseDouble(reader.readLine());
                Rectangle rectangle = new Rectangle(length, width);
                Triangle triangle = new Triangle(length, width, Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2)));
                System.out.println("Area of rectangle = " + rectangle.calculateArea());
                System.out.println("Perimeter of rectangle = " + rectangle.calculatePerimeter());
                System.out.println("Area from 2 triangles = " + triangle.calculateArea());
                System.out.println("Perimeter from 2 triangle = " + triangle.calculatePerimeter());
                break;
            }
        }
    }
}