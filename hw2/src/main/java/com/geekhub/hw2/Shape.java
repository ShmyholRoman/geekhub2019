package com.geekhub.hw2;

public interface Shape {
    double calculateArea();

    double calculatePerimeter();
}