package com.geekhub.hw2;

public enum Shapes {
    CIRCLE,
    SQUARE,
    RECTANGLE,
    TRIANGLE
}