package com.geekhub.hw2;

public class Rectangle extends Triangle implements Shape {
    private double length, width;

    Rectangle(double length, double width) {
        super(length, width, Math.sqrt(Math.pow(length, 2) + Math.pow(width, 2)));
        this.length = length;
        this.width = width;
    }

    public double calculateArea() {
        return length * width;
    }

    public double calculatePerimeter() {
        return 2 * width + 2 * length;
    }

}

