package com.geekhub.hw2;

public class Square extends Triangle {
    private double a;

    Square(double a) {
        super(a, a, Math.sqrt(Math.pow(a, 2) + Math.pow(a, 2)));
        this.a = a;
    }

    public double calculateArea() {
        return Math.pow(a, 2);
    }

    public double calculatePerimeter() {
        return 4 * a;
    }
}