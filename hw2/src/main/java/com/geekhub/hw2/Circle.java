package com.geekhub.hw2;

public class Circle implements Shape {
    private double radius;

    Circle(double radius) {
        this.radius = radius;
    }

    public double calculateArea() {
        return Math.pow(this.radius, 2) * Math.PI;
    }

    public double calculatePerimeter() {
        return Math.PI * this.radius * 2;
    }
}