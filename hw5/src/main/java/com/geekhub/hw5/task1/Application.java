package com.geekhub.hw5.task1;

import com.geekhub.hw5.task1.shop.Inventory;
import com.geekhub.hw5.task1.shop.Product;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Inventory inventory = new Inventory();
        List<Product> products = new ArrayList<>();
        products.add(new Product("Soccer ball", 20, 140));
        products.add(new Product("Boxing gloves", 50, 50));
        products.add(new Product("Swimming flippers", 15, 20));

        while (true) {
            System.out.println("***Data base of sport shop***");
            menu();
            int operation = Integer.parseInt(reader.readLine());

            switch (operation) {
                case 1:
                    System.out.println("Please, input title of product: ");
                    String title = reader.readLine();
                    System.out.println("Please, input price of product: ");
                    double price = Double.parseDouble(reader.readLine());
                    System.out.println("Please, input quantity of product: ");
                    int quantity = Integer.parseInt(reader.readLine());

                    products.add(new Product(title, price, quantity));

                    break;
                case 2:
                    System.out.println("Count of product: " + products.stream().count());
                    System.out.println();
                    for (Product element : products) {
                        System.out.println("Title: " + element.getTitle() + "\n" + "Price: " + element.getPrice()
                                + "\n" + "Quantity: " + element.getQuantity() + "\n");
                    }
                    break;
                case 3:
                    System.out.println("Price of all products = " + inventory.priceOfAll(products) + "$");
                    break;
            }

            if (operation == 4) {
                break;
            }
        }
    }

    private static void menu() {
        System.out.println("Please, input number of operation:");
        System.out.println("1. Add product to data base.");
        System.out.println("2. See all products in data base");
        System.out.println("3. See price of all products.");
        System.out.println("4. Exit");
    }
}
