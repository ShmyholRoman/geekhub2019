package com.geekhub.hw5.task1.shop;

import java.util.List;

public class Inventory {
    private static double priceOfAll = 0;

    public double priceOfAll(List<Product> list) {
        for (Product product : list) {
            priceOfAll = priceOfAll + product.getPrice() * product.getQuantity();
        }

        return priceOfAll;
    }
}
