package com.geekhub.hw5.task1.shop;

public class Product {
    private double price;
    private int quantity;
    private String title;

    public Product() {
        this.title = null;
        this.price = 0;
        this.quantity = 0;
    }

    public Product(String title, double price, int count) {
        this.title = title;
        this.price = price;
        this.quantity = count;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getTitle() {
        return title;
    }
}
