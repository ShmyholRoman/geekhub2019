package com.geekhub.hw5.task3;

import java.util.*;
import java.util.function.*;

public class CollectionUtils {

    private CollectionUtils() {
    }

    public static <E> List<E> generate(Supplier<E> generator,
                                       Supplier<List<E>> listFactory,
                                       int count) {
        List<E> list = listFactory.get();
        for (int i = 0; i < count; i++) {
            list.add(generator.get());
        }

        return list;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                list.add(element);
            }
        }

        return list;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }

        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }

        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return false;
            }
        }

        return true;
    }

    public static <T, R> List<R> map(List<T> elements,
                                     Function<T, R> mappingFunction,
                                     Supplier<List<R>> listFactory) {
        List<R> list = listFactory.get();
        for (T e : elements) {
            list.add(mappingFunction.apply(e));
        }

        return list;
    }

    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E max = iterator.next();
        while (iterator.hasNext()) {
            E element = iterator.next();
            if (comparator.compare(max, element) < 0) {
                max = element;
            }
        }

        return Optional.of(max);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E min = iterator.next();
        while (iterator.hasNext()) {
            E element = iterator.next();
            if (comparator.compare(min, element) > 0) {
                min = element;
            }
        }

        return Optional.of(min);
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> list = listFactory.get();
        Set<E> set = new HashSet<>();
        Iterator<E> iterator = elements.iterator();
        while (iterator.hasNext()) {
            E element = iterator.next();
            if (set.add(element)) {
                list.add(element);
            }
        }
        return list;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E e : elements) {
            consumer.accept(e);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Iterator<E> iterator = elements.iterator();
        E sum = iterator.next();

        while (iterator.hasNext()) {
            E element = iterator.next();
            sum = accumulator.apply(sum, element);
        }

        return Optional.of(sum);
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;

        for (E element : elements) {
            result = accumulator.apply(result, element);
        }

        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                        Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {

        Map<Boolean, List<E>> map = mapFactory.get();
        map.put(Boolean.TRUE, listFactory.get());
        map.put(Boolean.FALSE, listFactory.get());

        for (E e : elements) {
            map.get(predicate.test(e)).add(e);
        }

        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                                 Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory,
                                                 Supplier<List<T>> listFactory) {
        Map<K, List<T>> map = mapFactory.get();

        for (T e : elements) {
            if (!map.containsKey(classifier.apply(e))) {
                map.put(classifier.apply(e), listFactory.get());
            }
            map.get(classifier.apply(e)).add(e);
        }

        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction,
                                            Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();

        for (T e : elements) {
            K key = keyFunction.apply(e);
            if (map.containsKey(key)) {
                map.put(key, mergeFunction.apply(map.get(key), valueFunction.apply(e)));
            } else {
                map.put(key, valueFunction.apply(e));
            }
        }

        return map;
    }

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                        Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        Map<Boolean, List<T>> map = mapFactory.get();
        map.put(Boolean.TRUE, listFactory.get());
        map.put(Boolean.FALSE, listFactory.get());

        for (E e : elements) {
            map.get(predicate.test(e)).add(elementMapper.apply(e));
        }

        return map;
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                                 Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        Map<K, List<U>> map = mapFactory.get();

        for (T e : elements) {
            K key = classifier.apply(e);
            if (!map.containsKey(key)) {
                map.put(key, listFactory.get());
            }
            map.get(key).add(elementMapper.apply(e));
        }

        return map;
    }
}
