package com.geekhub.hw5.task3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 1, 4, 2, 1, 5, 5, 3, 22, 564, 2);
        List<Integer> list10 = new ArrayList<>();

        System.out.println("Check generate");
        List<Integer> list1 = Stream.generate(() -> 1).limit(10).collect(Collectors.toList());
        System.out.println(list1);
        List<Integer> list2 = CollectionUtils.generate(() -> 1, ArrayList::new, 10);
        System.out.println(list2);
        System.out.println();
        System.out.println("Check filter");
        List<Integer> list3 = list.stream().filter(s -> s < 20).collect(Collectors.toList());
        System.out.println(list3);
        List<Integer> list4 = CollectionUtils.filter(list, s -> s < 20);
        System.out.println(list4);
        System.out.println();
        System.out.println("Check anyMatch");
        boolean isElement = list.stream().anyMatch(s -> s == 4);
        System.out.println(isElement);
        boolean isElement1 = CollectionUtils.anyMatch(list, s -> s == 4);
        System.out.println(isElement1);
        System.out.println();
        System.out.println("Check allMatch");
        boolean isElement3 = list.stream().allMatch(s -> s < 10);
        System.out.println(isElement3);
        boolean isElement4 = CollectionUtils.allMatch(list, s -> s < 10);
        System.out.println(isElement4);
        System.out.println();
        System.out.println("Check noneMatch");
        boolean isElement5 = list.stream().noneMatch(s -> s == 432);
        System.out.println(isElement5);
        boolean isElement6 = CollectionUtils.noneMatch(list, s -> s == 432);
        System.out.println(isElement6);
        System.out.println();
        System.out.println("Check map");
        List<Integer> list5 = list.stream().map(s -> s * 10).collect(Collectors.toList());
        System.out.println(list5);
        List<Integer> list6 = CollectionUtils.map(list, s -> s * 10, ArrayList::new);
        System.out.println(list6);
        System.out.println();
        System.out.println("Check max");
        Optional<Integer> optional = list.stream().max(Comparator.comparingInt(m -> m));
        System.out.println(optional);
        Optional<Integer> optional1 = CollectionUtils.max(list, Comparator.comparingInt(m -> m));
        System.out.println(optional1);
        System.out.println();
        System.out.println("Check min");
        Optional<Integer> optional2 = list.stream().min(Comparator.comparingInt(m -> m));
        System.out.println(optional2);
        Optional<Integer> optional3 = CollectionUtils.min(list, Comparator.comparingInt(m -> m));
        System.out.println(optional3);
        System.out.println();
        System.out.println("Check distinct");
        List<Integer> list7 = list.stream().distinct().collect(Collectors.toList());
        System.out.println(list7);
        List<Integer> list8 = CollectionUtils.distinct(list, ArrayList::new);
        System.out.println(list8);
        System.out.println();
        System.out.println("Check forEach");
        list.stream().forEach(System.out::print);
        System.out.println();
        CollectionUtils.forEach(list, System.out::print);
        System.out.println();
        System.out.println();
        System.out.println("Check reduce");
        Optional<Integer> optional4 = list.stream().reduce(Integer::sum);
        System.out.println(optional4);
        Optional<Integer> optional5 = CollectionUtils.reduce(list, Integer::sum);
        System.out.println(optional5);
        System.out.println();
        System.out.println("Check reduce with seed");
        Integer sum = list.stream().reduce(10, Integer::sum);
        System.out.println(sum);
        Integer sum1 = CollectionUtils.reduce(10, list, Integer::sum);
        System.out.println(sum1);
        System.out.println();
        System.out.println("Check partitionBy");
        Map<Boolean, List<Integer>> map = list.stream().collect(Collectors.partitioningBy(s -> s < 110));
        System.out.println(map);
        Map<Boolean, List<Integer>> map1 = CollectionUtils.partitionBy(list10, s -> s < 110, HashMap::new, ArrayList::new);
        System.out.println(map1);
        System.out.println();
        System.out.println("Check groupBy");
        Map<Double, List<Integer>> map2 = list.stream().collect(Collectors.groupingBy(s -> s * 4.43));
        System.out.println(map2);
        Map<Double, List<Integer>> map3 = CollectionUtils.groupBy(list, s -> s * 4.43, HashMap::new, ArrayList::new);
        System.out.println(map3);
        System.out.println();
        System.out.println("Check toMap");
        Map<Integer, Integer> map4 = list.stream().collect(Collectors.toMap(s -> s * 2, s -> s * 4, Integer::sum));
        System.out.println(map4);
        Map<Integer, Integer> map5 = CollectionUtils.toMap(list, s -> s * 2, s -> s * 4, Integer::sum, HashMap::new);
        System.out.println(map5);
        System.out.println();
        System.out.println("Check partitionByAndMapElement");
        Map<Boolean, List<Integer>> map6 = list.stream()
                .collect(Collectors.partitioningBy((e) -> e % 2 == 0, Collectors.mapping(e -> e * 3, Collectors.toList())));
        System.out.println(map6);
        Map<Boolean, List<Integer>> map7 = CollectionUtils
                .partitionByAndMapElement(list, r -> r % 2 == 0, HashMap::new, ArrayList::new, e -> e * 3);
        System.out.println(map7);
        System.out.println();
        System.out.println("Check groupByAndMapElement");
        Map<Integer, List<Integer>> map8 = list.stream()
                .collect(Collectors.groupingBy((e) -> e, Collectors.mapping(e -> e * 2, Collectors.toList())));
        System.out.println(map8);
        Map<Integer, List<Integer>> map9 = CollectionUtils
                .groupByAndMapElement(list, e -> e, HashMap::new, ArrayList::new, e -> e * 2);
        System.out.println(map9);

    }
}
