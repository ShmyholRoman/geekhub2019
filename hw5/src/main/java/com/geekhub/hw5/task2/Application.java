package com.geekhub.hw5.task2;

public class Application {

    public static void main(String[] args) {
        User[] user = new User[]{new User("Roma", "34"), new User("Roma", "22"), new User("Sasha", "32"),
                new User("Roma", "34")};
        User[] userBubble, userInsertion, userSelection;

        userBubble = ArraySorter.bubbleSort(user, Direction.DESC);
        System.out.println("Bubble sort");
        for (User u : user) {
            System.out.println(u.toString());
        }
        System.out.println();
        for (User u : userBubble) {
            System.out.println(u.toString());
        }
        System.out.println();
        userInsertion = ArraySorter.insertionSort(user, Direction.DESC);
        System.out.println("Insertion sort");
        for (User u : user) {
            System.out.println(u.toString());
        }
        System.out.println();
        for (User u : userInsertion) {
            System.out.println(u.toString());
        }
        System.out.println();
        System.out.println("Selection sort");
        userSelection = ArraySorter.selectionSort(user, Direction.ASC);
        for (User u : user) {
            System.out.println(u.toString());
        }
        System.out.println();
        for (User u : userSelection) {
            System.out.println(u.toString());
        }
    }
}
