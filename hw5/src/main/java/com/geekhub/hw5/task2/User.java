package com.geekhub.hw5.task2;

public class User implements Comparable<User> {
    private final String name;
    private final String age;

    public User(String name, String age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(User user) {
        return this.name.compareTo(user.name) != 0 ? this.name.compareTo(user.name) : this.age.compareTo(user.age);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}

