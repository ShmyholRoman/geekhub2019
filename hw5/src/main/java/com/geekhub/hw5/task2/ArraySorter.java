package com.geekhub.hw5.task2;

public class ArraySorter {

    public static <T extends Comparable<T>> T[] bubbleSort(T[] array, Direction direction) {
        T[] arrayNew = array.clone();
        int size = array.length;
        int k = direction.getDirection(direction);
        T tmp;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if ((arrayNew[j].compareTo(arrayNew[j + 1])) * k > 0) {
                    tmp = arrayNew[j + 1];
                    arrayNew[j + 1] = arrayNew[j];
                    arrayNew[j] = tmp;
                }
            }
        }

        return arrayNew;
    }

    public static <T extends Comparable<T>> T[] insertionSort(T[] array, Direction direction) {
        T[] arrayNew = array.clone();
        int size = array.length;
        int k = direction.getDirection(direction);
        T tmp;
        for (int i = 0; i < size; i++) {
            tmp = arrayNew[i];
            int j = i;
            while (j > 0 && arrayNew[j - 1].compareTo(tmp) * k > 0) {
                arrayNew[j] = arrayNew[j - 1];
                j--;

            }
            arrayNew[j] = tmp;
        }

        return arrayNew;
    }

    public static <T extends Comparable<T>> T[] selectionSort(T[] array, Direction direction) {
        T[] arrayNew = array.clone();
        int size = array.length;
        int k = direction.getDirection(direction);
        int minimum;
        T tmp;
        for (int i = 0; i < size - 1; i++) {
            minimum = i;
            for (int j = i + 1; j < size; j++) {
                if (arrayNew[minimum].compareTo(arrayNew[j]) * k > 0) {
                    minimum = j;
                }
            }
            tmp = arrayNew[minimum];
            arrayNew[minimum] = arrayNew[i];
            arrayNew[i] = tmp;

        }

        return arrayNew;
    }
}
