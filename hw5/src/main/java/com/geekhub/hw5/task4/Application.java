package com.geekhub.hw5.task4;

import com.geekhub.hw5.task4.task.Task;
import com.geekhub.hw5.task4.task.TaskType;
import com.geekhub.hw5.task4.task.scheduler.Scheduler;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        List<Task> list = new ArrayList<>();
        Scheduler scheduler = new Scheduler();
        list.add(new Task(0, TaskType.IMPORTANT, "Task1",
                true, "category1", LocalDate.now().minusDays(2)));
        list.add(new Task(1, TaskType.IMPORTANT, "Task2",
                false, "category2", LocalDate.now().minusDays(13)));
        list.add(new Task(2, TaskType.NOT_IMPORTANT, "Task3",
                true, "category3", LocalDate.now().minusDays(13)));
        list.add(new Task(3, TaskType.NOT_IMPORTANT, "Task4",
                false, "category4", LocalDate.now().minusDays(13)));
        list.add(new Task(4, TaskType.IMPORTANT, "Task5",
                false, "category5", LocalDate.now().minusDays(31)));
        list.add(new Task(5, TaskType.NOT_IMPORTANT, "Task2",
                true, "category2", LocalDate.now().minusDays(4)));
        list.add(new Task(6, TaskType.IMPORTANT, "Task1",
                false, "category1", LocalDate.now().minusDays(6)));
        list.add(new Task(7, TaskType.NOT_IMPORTANT, "Task2",
                true, "category2", LocalDate.now().minusDays(5)));
        list.add(new Task(8, TaskType.NOT_IMPORTANT, "Task5",
                false, "category5", LocalDate.now().minusDays(4)));
        list.add(new Task(9, TaskType.IMPORTANT, "Task3",
                false, "category3", LocalDate.now().minusDays(3)));
        list.add(new Task(10, TaskType.IMPORTANT, "Task4",
                false, "category4", LocalDate.now().minusDays(1)));
        list.add(new Task(11, TaskType.NOT_IMPORTANT, "Task4",
                false, "category4", LocalDate.now().minusDays(2)));
        list.add(new Task(12, TaskType.NOT_IMPORTANT, "Task4",
                false, "category4", LocalDate.now().minusDays(1)));
        list.add(new Task(13, TaskType.IMPORTANT, "Task4",
                false, "category12", LocalDate.now()));

        System.out.println("Result of find5NearestImportantTasks: " + scheduler.find5NearestImportantTasks(list));
        System.out.println();
        System.out.println("Result of getUniqueCategories: " + scheduler.getUniqueCategories(list));
        System.out.println();
        System.out.println("Result of getCategoriesWithTasks: " + scheduler.getCategoriesWithTasks(list));
        System.out.println();
        System.out.println("Result of splitTasksIntoDoneAndInProgress: " + scheduler.splitTasksIntoDoneAndInProgress(list));
        System.out.println();
        System.out.println("Result of existsTaskOfCategory: " + scheduler.existsTaskOfCategory(list, "category6"));
        System.out.println("Result of existsTaskOfCategory: " + scheduler.existsTaskOfCategory(list, "category2"));
        System.out.println();
        System.out.println("Result of getTitlesOfTasks: " + scheduler.getTitlesOfTasks(list, 4, 7));
        System.out.println();
        System.out.println("Result of getCountsByCategories: " + scheduler.getCountsByCategories(list));
        System.out.println();
        System.out.println("Result of getCategoriesNamesLengthStatistics: " + scheduler.getCategoriesNamesLengthStatistics(list));
        System.out.println();
        System.out.println("Result of findTaskWithBiggestCountOfCategories: " + scheduler.findTaskWithBiggestCountOfCategories(list));

    }
}
