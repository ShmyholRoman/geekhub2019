package com.geekhub.hw5.task4.task;

import java.time.LocalDate;

public class Task {
    private int id;
    private TaskType type;
    private String title;
    private boolean done;
    private String category;
    private LocalDate startsOn;

    public Task(int id, TaskType type, String title, boolean done, String categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.category = categories;
        this.startsOn = startsOn;
    }

    public TaskType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

    public LocalDate getStartsOn() {
        return startsOn;
    }

    public String getCategory() {
        return this.category;
    }

    @Override
    public String toString() {
        return "[" + id + " " + type + " " + title + " " + done + " " + category + " " + startsOn + ']';
    }
}
