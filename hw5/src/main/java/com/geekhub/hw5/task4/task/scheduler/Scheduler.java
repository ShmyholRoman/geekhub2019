package com.geekhub.hw5.task4.task.scheduler;

import com.geekhub.hw5.task4.task.Task;
import com.geekhub.hw5.task4.task.TaskType;

import java.util.Comparator;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Scheduler {

    public List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(t -> t.getType().equals(TaskType.IMPORTANT) && !t.isDone())
                .sorted(Comparator.comparing(Task::getStartsOn).reversed())
                .limit(5)
                .collect(Collectors.toList());
    }

    public List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .map(Task::getCategory)
                .distinct()
                .collect(Collectors.toList());
    }

    public Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(Task::getCategory));
    }

    public Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.partitioningBy(Task::isDone));
    }

    public boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .anyMatch(task -> task.getCategory().equals(category));
    }

    public String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .map(Task::getTitle)
                .limit(endNo)
                .skip(startNo)
                .collect(Collectors.joining(", "));
    }

    public Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(Task::getCategory, Collectors.counting()));
    }

    public IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.summarizingInt(t -> t.getCategory().length()));
    }

    public Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparing(t -> t.getCategory().length()))
                .orElse(null);
    }
}
