package com.geekhub.hw3;

public abstract class Actives {

    private double usd;
    private double uah;
    double depositUsd;
    double depositUah;
    private double percentUsd = 0.75;
    private double rateUsd = 25.4;

    public void depositUsd(int countYears) {
        depositUsd = usd + usd * countYears * percentUsd;
    }

    public void depositUah(int countYears) {
        double percentUah = 2.5;
        depositUah = uah + uah * countYears * percentUah;
    }

    public double convertUsdToUah(double countUsd) {
        usd = usd - countUsd;
        uah = uah + countUsd * rateUsd;
        return uah;
    }

    public void setUah(double uah) {
        this.uah = uah;
    }

    public void setUsd(double usd) {
        this.usd = usd;
    }
}
