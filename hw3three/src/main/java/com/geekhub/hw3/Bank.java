package com.geekhub.hw3;


import java.lang.*;

import java.util.ArrayList;

public class Bank extends Actives {
    double usd;
    private double uah;
    private ArrayList<Customer> customers = new ArrayList<>();

    Bank(Customer customer) {
        customers.add(customer);
        setDepositUah(0);
        setDepositUsd(0);
    }

    Bank(Customer customer, double usd, double uah) {
        customers.add(customer);
        setDepositUah(uah);
        setDepositUsd(usd);
    }

    private void setDepositUsd(double deposit) {
        usd = deposit;
    }

    private void setDepositUah(double deposit) {
        uah = deposit;
    }

    public String toString() {
        return customers.toString() + "\n" + "Usd: " + usd + " Uah " + uah;
    }


}
