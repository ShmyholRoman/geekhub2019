package com.geekhub.hw3;

public class Customer extends Actives {
    private String firstName, lastName, sex, phoneNumber;
    private Integer age, idNumber;

    Customer() {
        firstName = "Undefined";
        lastName = "Undefined";
        sex = "Undefined";
        phoneNumber = "Undefined";
        age = null;
        idNumber = null;
    }

    Customer(String firstName, String lastName, String sex, String phoneNumber, Integer age, Integer idNumber) {
        setFirstName(firstName);
        setLastName(lastName);
        setSex(sex);
        setPhoneNumber(phoneNumber);
        setAge(age);
        setIdNumber(idNumber);
    }

    private String getFirstName() {
        return firstName;
    }

    private void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    private String getLastName() {
        return lastName;
    }

    private void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private String getSex() {
        return sex;
    }

    private void setSex(String sex) {
        this.sex = sex;
    }

    private String getPhoneNumber() {
        return phoneNumber;
    }

    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private Integer getAge() {
        return age;
    }

    private void setAge(Integer age) {
        this.age = age;
    }

    private Integer getIdNumber() {
        return idNumber;
    }

    private void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public String toString() {
        return ("\n" + "First name: " + getFirstName() + "\n" + " Last name: " + getLastName()
                + "\n" + " Phone number: " + getPhoneNumber() + "\n" + " Sex: " + getSex()
                + " Age: " + getAge() + "\n" + " Identification number: " + getIdNumber());
    }


}
