package com.geekhub.hw3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ApplicationTest {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        menu();
        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(new Customer("John", "Bridge", "male", "+1334565544", 32, 23322222));
        customers.add(new Customer("Sarah", "Rose", "female", "+465465678", 21, 75678655));
        customers.add(new Customer());
        ArrayList<Bank> bank = new ArrayList<>();
        bank.add(new Bank(customers.get(0), 1010003, 4324543));
        bank.add(new Bank(customers.get(1)));
        int numberOperation;

        while (true) {
            numberOperation = Integer.parseInt(reader.readLine());
            if (numberOperation == 5) {
                break;
            }
            switch (numberOperation) {
                case 1:
                    for (Customer customer : customers) {
                        System.out.println(customer.toString());
                    }

                    break;
                case 2:
                    double usd, uah;
                    int id;
                    System.out.println("Input first name of customer for enter the money:");
                    id = Integer.parseInt(reader.readLine());
                    System.out.println("Input how much dollars you want enter to bank: ");
                    usd = Double.parseDouble(reader.readLine());
                    System.out.println("Input how much hryvnia you want enter to bank: ");
                    uah = Double.parseDouble(reader.readLine());
                    bank.get(id).setUah(uah);
                    bank.get(id).setUsd(usd);

                    break;
                case 3:
                    for (Bank value : bank) {
                        System.out.println(value.toString());
                    }

                    break;
                case 4:
                    double sum = 0;
                    for (Bank value : bank) {
                        sum += value.convertUsdToUah(value.usd);
                    }
                    System.out.println("Price of all actives " + sum + " UAH");

                    break;
                default:
                    System.out.println("Incorrect input!");
                    break;
            }
            menu();
        }
    }

    private static void menu() {
        System.out.println("Please input number of your operation.");
        System.out.println("1. View info about customers.");
        System.out.println("2. Add customer's actives.");
        System.out.println("3. View info about customer's actives.");
        System.out.println("4. Calculate whole price of actives in the bank.");
        System.out.println("5. Exit.");
    }
}
