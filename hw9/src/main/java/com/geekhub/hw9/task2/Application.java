package com.geekhub.hw9.task2;

import java.io.IOException;
import java.util.Scanner;

public class Application {
    private static int numberOfThreads = Runtime.getRuntime().availableProcessors();

    public static void main(String[] args) throws IOException {

        ImageCrawler imageCrawler = new ImageCrawler(numberOfThreads, "/users/roman/desktop/geekhub/");
        imageCrawler.downloadImages("http://ua.korzonews.info/%D1%89%D0%BE%D0%B4%D0%B5%D0%BD%D0%BD%D1%96-%D0%BF%D1%80%D0%B8%D0%BA%D0%BE%D0%BB%D1%8C%D0%BD%D1%96-%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B8/");

        System.out.println("While it's loading you can enter another url to start download images:");

        Scanner scanner = new Scanner(System.in);
        String command;
        while (!"exit".equals(command = scanner.next())) {
            imageCrawler.downloadImages(command);
            System.out.println("...and another url:");
        }
        imageCrawler.stop();
    }
}
