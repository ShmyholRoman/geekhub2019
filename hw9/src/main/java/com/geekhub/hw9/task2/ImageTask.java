package com.geekhub.hw9.task2;

import java.io.*;
import java.net.URL;

/**
 * Represents worker that downloads image from URL to specified folder.<br/>
 * Name of the image will be constructed based on URL. Names for the same URL will be the same.
 */
public class ImageTask implements Runnable {

    private final URL url;
    private final String folder;

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    public ImageTask(URL url, String folder) {
        this.url = url;
        this.folder = folder;
    }

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    @Override
    public void run() {
        try (OutputStream out = new BufferedOutputStream(new FileOutputStream(folder + buildFileName(url)))) {
            out.write(ConnectionUtils.getData(url));
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    private String buildFileName(URL url) {
        return url.toString().replaceAll("[^a-zA-Z0-9-_.]", "_");
    }
}
