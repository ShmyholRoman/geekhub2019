package com.geekhub.hw9.task2;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.concurrent.*;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    private final ExecutorService executorService;
    private final String folder;

    public ImageCrawler(int numberOfThreads, String folder) {
        this.folder = folder;
        executorService = Executors.newFixedThreadPool(numberOfThreads);
    }

    /**
     * Call this method to start download images from specified URL.
     *
     * @param urlToPage
     * @throws java.io.IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        Collection<URL> urls = page.getImageLinks();
        for (URL url : urls) {
            if (isImageURL(url)) {
                ImageTask imageTask = new ImageTask(url, folder);
                executorService.submit(imageTask);
            }
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            executorService.shutdownNow();
        }
    }

    private boolean isImageURL(URL url) {
        return url.toString().matches("([^\\s]+(\\.(?i)(jpg|png))$)");
    }
}