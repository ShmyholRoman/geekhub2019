package com.geekhub.hw9.task2;

import java.io.*;
import java.net.URL;

public class ConnectionUtils {

    private ConnectionUtils() {
    }

    public static byte[] getData(URL url) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (InputStream in = new BufferedInputStream(url.openStream())) {
            int bytesRead;
            byte[] content = new byte[2048];
            while ((bytesRead = in.read(content)) > 0) {
                out.write(content, 0, bytesRead);
            }
        }

        return out.toByteArray();
    }
}
