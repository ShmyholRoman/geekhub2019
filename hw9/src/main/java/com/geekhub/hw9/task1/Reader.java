package com.geekhub.hw9.task1;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Reader {
    public static List<String> readerOfFile(File file) throws IOException {
        List<String> result = new ArrayList<>();
        String line;
        try (BufferedReader reader = new BufferedReader(new FileReader(file.getPath()))) {
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
        }

        return result;
    }

    public static String readPage(String str) throws IOException {
        String text;
        StringBuilder textPage = new StringBuilder();
        URL url = new URL(str);
        try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
            while ((text = in.readLine()) != null) {
                textPage.append(text);
            }
        }

        return textPage.toString();
    }
}
