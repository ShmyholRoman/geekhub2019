package com.geekhub.hw9.task1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        File file = new File("/Users/roman/IdeaProjects/geekhub2019/hw9/resourse_and_result/http.txt");
        List<String> urls = Reader.readerOfFile(file);
        List<String> hashes = new ArrayList<>();
        for (String str : urls) {
            Future<String> future = executorService.submit(() -> Reader.readPage(str));
            hashes.add(future.get());
        }
        Encryption.write(hashes);
        executorService.shutdown();
    }
}
