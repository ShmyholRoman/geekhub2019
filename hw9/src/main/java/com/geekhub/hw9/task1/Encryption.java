package com.geekhub.hw9.task1;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.*;
import java.util.List;

public class Encryption {
    public static void write(List<String> list) throws IOException {
        File result = new File("/Users/roman/IdeaProjects/geekhub2019/hw9/resourse_and_result/result.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(result))) {
            for (String str : list) {
                String md5 = md5Apache(str);
                bufferedWriter.write(md5);
                bufferedWriter.newLine();
            }
        }
    }

    public static String md5Apache(String st) {
        return DigestUtils.md5Hex(st);
    }
}
